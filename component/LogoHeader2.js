import React from "react";
import { View,Image,Linking } from "react-native";
import {Button} from 'native-base';

export default class LogoTitle extends React.Component {
  render() {
    return (
        <View style={{flex:1,flexDirection:'row', justifyContent:'flex-end'}}>
            <Image
              source={require('../assets/kop-ppid-rev.png')}
              style={{ width:325,height: 57,left:-30}}
            />
        </View>
    );
  }
}