import React from "react";
import { StyleSheet, View,Image,ImageBackground,Dimensions,TouchableOpacity } from "react-native";
import { Container, Header, Content, Card, CardItem, Text, Body } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import * as Font from 'expo-font';
import { Ionicons } from '@expo/vector-icons';
import LogoHeader2 from '../component/LogoHeader2'; 

export default class Faq extends React.Component {
  static navigationOptions = {
    headerTitle: <LogoHeader2 />,
    headerStyle: {
      backgroundColor:'white',
      paddingBottom:20,
      height:88
    },
  };

  constructor(props) {
    super(props);
    this.state = { loading: true };
  }

  async componentDidMount() {
    await Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf')
    });
    this.setState({ loading: false });
  }
  
  render() {
    if (this.state.loading) {
      return (
        <Text>Sedang memuat....</Text>
      );
    }
    return (
      <Container>
        <Content>
          <Card>
            <CardItem header style={{backgroundColor:'#16529b',paddingBottom:5,paddingTop:5}}>
              <Text style={{color:'white', fontSize:14}}>Siapa yang dapat mengajukan permohonan informasi publik?</Text>
            </CardItem>
            <CardItem>
              <Body>
                <Text>
                  Setiap warga negara dan/atau badan hukum Indonesia sebagaimana diatur dalam Undang-Undang Republik Indonesia Nomor 14 Tahun 2008 tentang Keterbukaan Informasi Publik.
                </Text>
              </Body>
            </CardItem>
         </Card>

         <Card>
            <CardItem header style={{backgroundColor:'#16529b',paddingBottom:5,paddingTop:5}}>
              <Text style={{color:'white', fontSize:14}}>Apakah persyaratan penyampaian permohonan informasi publik dan pengajuan keberatan?</Text>
            </CardItem>
            <CardItem>
              <Body>
                <Text>
                  Melampirkan Kartu Tanda Penduduk untuk pemohon perorangan atau bukti pengesahan badan hukum yang diterbitkan oleh kementerian yang membidangi urusan hukum dan asasi manusia untuk pemohon badan hukum Indonesia.
                </Text>
              </Body>
            </CardItem>
         </Card>

         <Card>
            <CardItem header style={{backgroundColor:'#16529b',paddingBottom:5,paddingTop:5}}>
              <Text style={{color:'white', fontSize:14}}>Bagaimana cara mendaftar sebagai pengguna layanan informasi publik BP2MI?</Text>
            </CardItem>
            <CardItem>
              <Body>
                <Text>
                  Melakukan registrasi terlebih dahulu pada: aplikasi web BP2MI melalui menu Hubungi Kami, Formulir Pemohon Informasi; aplikasi mobile, melalui menu Layanan, klik Daftar Baru. Melengkapi kolom yang telah disediakan dan melampirkan dokumen pendukung yang dipersyaratkan; Apabila data pengguna sudah lengkap, pengguna akan menerima email konfirmasi bahwa pengguna sudah terdaftar sebagai pengguna layanan informasi publik BP2MI.
                </Text>
              </Body>
            </CardItem>
         </Card>

         <Card>
            <CardItem header style={{backgroundColor:'#16529b',paddingBottom:5,paddingTop:5}}>
              <Text style={{color:'white', fontSize:14}}>Bagaimana cara menyampaikan permohonan informasi publik?</Text>
            </CardItem>
            <CardItem>
              <Body>
                <Text>
                  Permohonan informasi dapat disampaikan melalui: aplikasi web BP2MI melalui menu Hubungi Kami, Formulir Pemohon Informasi; aplikasi mobile, melalui menu Layanan, klik Permohonan Informasi. Melengkapi kolom yang telah disediakan dan melampirkan dokumen pendukung yang dipersyaratkan; Apabila data permohonan informasi sudah lengkap, pengguna akan menerima email konfirmasi bahwa permohonan informasi sudah diterima dan sedang diproses. 
                </Text>
              </Body>
            </CardItem>
         </Card>

         <Card>
            <CardItem header style={{backgroundColor:'#16529b',paddingBottom:5,paddingTop:5}}>
              <Text style={{color:'white', fontSize:14}}>Bagaimana mekanisme pemberian tanggapan BP2MI atas permohonan informasi publik?</Text>
            </CardItem>
            <CardItem>
              <Body>
                <Text>
                  Tanggapan atas permohonan informasi publik akan disampaikan dengan cara memberikan langsung ke pemohon, faksimili, dikirim lewat pos, atau email yang telah didaftarkan pengguna pada saat registrasi.
                </Text>
              </Body>
            </CardItem>
         </Card>

         <Card>
            <CardItem header style={{backgroundColor:'#16529b',paddingBottom:5,paddingTop:5}}>
              <Text style={{color:'white', fontSize:14}}>Berapa lama pemberian tanggapan BP2MI atas permohonan informasi publik?</Text>
            </CardItem>
            <CardItem>
              <Body>
                <Text>
                 Tanggapan dari BP2MI akan disampaikan paling lambat 10 (sepuluh) hari kerja sejak permohonan informasi telah memenuhi persyaratan dan dapat diperpanjang 7 (tujuh) hari kerja berikutnya.
                </Text>
              </Body>
            </CardItem>
         </Card>

         <Card>
            <CardItem header style={{backgroundColor:'#16529b',paddingBottom:5,paddingTop:5}}>
              <Text style={{color:'white', fontSize:14}}>Bagaimana cara pengajuan keberatan atas informasi publik BP2MI?</Text>
            </CardItem>
            <CardItem>
              <Body>
                <Text>
                 Keberatan dapat diajukan melalui: aplikasi mobile, melalui menu Layanan, klik Pengajuan Keberatan. Melengkapi kolom yang telah disediakan; Melampirkan dokumen pendukung yang dipersyaratkan.
                </Text>
              </Body>
            </CardItem>
         </Card>

         <Card>
            <CardItem header style={{backgroundColor:'#16529b',paddingBottom:5,paddingTop:5}}>
              <Text style={{color:'white', fontSize:14}}>Berapa biaya untuk memperoleh informasi publik?</Text>
            </CardItem>
            <CardItem>
              <Body>
                <Text>
                 BP2MI menyediakan layanan informasi publik secara gratis (tidak dipungut biaya), kecuali untuk informasi yang telah ditentukan biayanya sesuai peraturan mengenai penerimaan negara bukan pajak. Sedangkan untuk penggandaan atau perekaman, pengguna layanan informasi publik dapat melakukan penggandaan sendiri di tempat penyediaan jasa fotocopy yang berada di sekitar gedung kantor pusat/kantor vertikal BP2MI dengan didampingi oleh petugas layanan informasi atau menyiapkan media perekaman elektronik lainnya untuk perekaman data informasinya.
                </Text>
              </Body>
            </CardItem>
         </Card>

         <Card>
            <CardItem header style={{backgroundColor:'#16529b',paddingBottom:5,paddingTop:5}}>
              <Text style={{color:'white', fontSize:14}}>Waktu layanan infromasi:</Text>
            </CardItem>
            <CardItem>
              <Body>
                <Text>
                 Layanan informasi publik dilaksanakan setiap hari kerja Senin s.d. Jumat dari pukul 08.00 WIB – 16.00 WIB.
                </Text>
              </Body>
            </CardItem>
         </Card>

         
        </Content>
      </Container>
    );
  }
}