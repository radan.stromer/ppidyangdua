import React, { Component } from 'react';
import {KeyboardAvoidingView,SafeAreaView,Alert } from "react-native";
import {Content, Form, Item, Input,Button,Text, Textarea,Picker } from 'native-base';
import LogoHeader2 from '../../component/LogoHeader2';
import GoogleReCaptcha from 'rn-google-recaptcha-v2';

const siteKey = '6LfZFdkUAAAAALib79ReeaQafUOl3u1ONFH-YO9N';
const baseUrl = 'http://dadan.id';

export default class Signup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      type : '',
      email : '',
      pass : '',
      ktp  : '',
      nama:'',
      nik:'',
      telp:'',
      alamat:'',
      pekerjaan : '',
      errorMessage: null,
      recaptchaViewHeight: 90, //initial default height
      showSubmit : false
    }
  }

  static navigationOptions = {
    headerTitle: <LogoHeader2 />,
    headerStyle: {
      backgroundColor:'white',
      paddingBottom:20,
      height:88
    },
  };

  handleSignUp = () => {
    if(this.state.email<6){
      alert('Password minimal 6 karakter');
      return
    }

    fetch(`${global.apiEndPoint}index.php/api/signup/signup`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        type      : this.state.type,
        nama      : this.state.nama,
        email     : this.state.email,
        password  : this.state.pass,
        alamat    : this.state.alamat,
        nik       : this.state.nik,
        pekerjaan : this.state.pekerjaan,
        telp      : this.state.telp,
      })
    }).then((response) => response.json())
      .then((responseJson) => {
        if(responseJson.status=='200'){ 
          this.props.navigation.navigate('Login');
        }else{
          //Alert.alert(responseJson.msg);
        }
        console.log(responseJson);
      }).catch((error) => {
        //Alert.alert(error);
        console.error(error); 
      });
  }

  onTypeChange(value: string) {
    this.setState({
      type: value
    });
  }

  render() {
    const { recaptchaViewHeight } = this.state;
    return (
            <Content padder>
              <Text style={{fontWeight:'bold', fontSize:14,marginBottom:10, textAlign:'justify'}}>Silahkan mendaftar untuk mengajukan permohonan informasi maupun pengajuan keberatan.</Text>
              {this.state.errorMessage &&
              <Text style={{ color: 'red' }}>
                {this.state.errorMessage}
              </Text>}
                <KeyboardAvoidingView behavior="padding" style={{ flex: 1,}} enabled>
                <Form>
                  <Text>Tipe Pendaftar (*)</Text>
                  <Item picker>
                    <Picker
                      mode="dropdown"
                      placeholder="Cara memperoleh Informasi"
                      placeholderStyle={{ color: "#bfc6ea" }}
                      placeholderIconColor="#007aff"
                      selectedValue={this.state.type}
                      onValueChange={this.onTypeChange.bind(this)}
                    >
                      <Picker.Item label="Pribadi" value="Pribadi" />
                      <Picker.Item label="Badan hukum" value="Badan" />
                    </Picker>
                  </Item>
                  <Item regular style={{marginBottom:15}}>
                    <Input value={this.state.nik} placeholder="No KTP" onChangeText={nik=> this.setState({nik})} keyboardType="number-pad"/>
                  </Item>

                  <Item regular style={{marginBottom:15}}>
                    <Input value={this.state.nama} placeholder="Nama" onChangeText={nama=> this.setState({nama})}/>
                  </Item>

                  <Item regular style={{marginBottom:15}}>
                    <Input value={this.state.pekerjaan} placeholder="Pekerjaan" onChangeText={pekerjaan=> this.setState({pekerjaan})}/>
                  </Item>

                  <Textarea style={{marginBottom:15}} value={this.state.alamat} rowSpan={5} bordered placeholder="Alamat" onChangeText={alamat=> this.setState({alamat})}/>
                  
                  <Item regular style={{marginBottom:15}}>
                    <Input value={this.state.telp} placeholder="No Telp" onChangeText={telp=> this.setState({telp})} keyboardType="phone-pad"/>
                  </Item>

                  <Item regular style={{marginBottom:15}}>
                    <Input value={this.state.email} placeholder="Email" onChangeText={email=> this.setState({email})} keyboardType="email-address"/> 
                  </Item>
                  <Item regular style={{marginBottom:15}}>
                    <Input value={this.state.pass} placeholder="Password" secureTextEntry onChangeText={pass=> this.setState({pass})}/>
                  </Item>
                  <SafeAreaView style={{flex:1}}>
                      <GoogleReCaptcha
                          style={{ height: recaptchaViewHeight }}
                          siteKey={siteKey}
                          url={baseUrl}
                          languageCode="en"
                          onMessage={this.onRecaptchaEvent} />
                  </SafeAreaView>
                 
                  {this.state.showSubmit &&
                    <Button onPress={this.handleSignUp}>
                      <Text>
                        Daftar
                      </Text>
                    </Button>
                  }
                </Form>
            </KeyboardAvoidingView>

            </Content>
    );
  }


  onRecaptchaEvent = event => {
    if (event && event.nativeEvent.data) {
        const data = decodeURIComponent(
            decodeURIComponent(event.nativeEvent.data),
        );
        if (data.startsWith('CONTENT_PARAMS:')) {
            let params = JSON.parse(data.substring(15));
            let recaptchaViewHeight = params.visibility === 'visible' ? params.height : 90;
            this.setState({ recaptchaViewHeight });
        } else if (['cancel', 'error', 'expired'].includes(data)) {
            return;
        } else {
            console.log('Verified code from Google', data);
            this.setState({ recaptchaToken: data });
            this.setState({ showSubmit: true });
        }
    }
  };
}