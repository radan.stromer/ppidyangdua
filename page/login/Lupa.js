import React, { Component } from 'react';
import { View,Image } from "react-native";
import { Container, Header, Content, Tab, Tabs, Form, Item, Input,Button,Text, H1 } from 'native-base';
import LogoHeader2 from '../../component/LogoHeader2';


export default class Lupa extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email:'',
            otp:'',
            errorMessage: null,
            showOtp:false,
            showResetPass:false,
            showEmail:true,
            password:'',
            password2:'',
            user_id:'',
            loading:false,
        };
    }

    static navigationOptions = {
    headerTitle: <LogoHeader2 />,
    headerStyle: {
        backgroundColor:'white',
        paddingBottom:20,
        height:88
    },
    };

    handleLupa = () => {
        this.setState({
            loading: true
        });

        fetch(`${global.apiEndPoint}index.php/api/login/forgot`, {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              email     : this.state.email 
            })
          }).then((response) => response.json())
            .then((responseJson) => {
              if(responseJson.status==200){
                this.setState({
                    loading: false,
                    showEmail:false,
                    showOtp:true
                }); 
                alert(responseJson.msg);
              }else{
                alert(responseJson.msg);
              }
            }).catch((error) => {
              alert(error);
              console.error(error);
            });
    }

    submitOtp = () => {
        this.setState({
            loading: true
        });

        fetch(`${global.apiEndPoint}index.php/api/login/submit_otp`, {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              otp     : this.state.otp 
            })
          }).then((response) => response.json())
            .then((responseJson) => {
              if(responseJson.status==200){
                this.setState({
                    loading: false,
                    showOtp:false,
                    showResetPass:true,
                    user_id:responseJson.user_id
                }); 
                alert(responseJson.msg);
              }else{
                alert(responseJson.msg);
              }
            }).catch((error) => {
              alert(error);
              console.error(error);
            });
    }

    resetPassword = () => {
        this.setState({
            loading: true
        });

        fetch(`${global.apiEndPoint}index.php/api/login/reset_password`, {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              otp     : this.state.otp,
              user_id     : this.state.user_id,
              password : this.state.password,
              password2 : this.state.password2,
            })
          }).then((response) => response.json())
            .then((responseJson) => {
              if(responseJson.status==200){
                alert(responseJson.msg);
                this.props.navigation.navigate('Login');
              }else{
                alert(responseJson.msg);
              }
            }).catch((error) => {
              alert(error);
              console.error(error);
            });
    }

    render() {
    return ( 
        <Container style={{padding:10}}>
            {this.state.showEmail?(
                <>
                    <Text style={{fontWeight:'bold', fontSize:14,marginBottom:10, textAlign:'justify'}}>Masukan email terdaftar untuk mendapatkan kode OTP reset Password.</Text>
                    {this.state.errorMessage &&
                    <Text style={{ color: 'red' }}>
                        {this.state.errorMessage}
                    </Text>}
                    <Form>
                    <Item regular style={{marginBottom:15}}>
                        <Input onChangeText={email=>this.setState({email})} placeholder="Email" keyboardType="email-address"/>
                    </Item>
                    <Button full onPress={this.handleLupa}>
                        <Text>
                        Reset Password
                        </Text>
                    </Button>
                    {this.state.loading &&
                    <Text style={{ color: 'green' }}>
                    Loading
                    </Text>}
                    </Form>
                </>
            ):(
                <>
                {this.state.showOtp &&
                    <>
                        <Text style={{fontWeight:'bold', fontSize:14,marginBottom:10, textAlign:'justify'}}>Masukan kode OTP dar email untuk reset Password.</Text>
                        <Form>
                            <Item regular style={{marginBottom:15}}>
                                <Input onChangeText={otp=>this.setState({otp})} placeholder="Kode OTP" keyboardType="number"/>
                            </Item>
                            <Button full onPress={this.submitOtp}>
                                <Text>
                                Submit
                                </Text>
                                {this.state.loading &&
                                    <Text style={{ color: 'green' }}>
                                    Loading
                                </Text>}
                            </Button>
                        </Form>
                    </>
                }
                
                {this.state.showResetPass &&
                    <>
                        <Text style={{fontWeight:'bold', fontSize:14,marginBottom:10, textAlign:'justify'}}>Silahkan ketik Password baru Anda.</Text>
                        <Form>
                            <Item regular style={{marginBottom:15}}>
                                <Input onChangeText={password=>this.setState({password})} placeholder="Password" keyboardType="text"/>
                            </Item>
                            <Item regular style={{marginBottom:15}}>
                                <Input onChangeText={password2=>this.setState({password2})} placeholder="Ulangi Password" keyboardType="text"/>
                            </Item>
                            <Button full onPress={this.resetPassword}>
                                <Text>
                                Submit
                                </Text>
                                {this.state.loading &&
                                    <Text style={{ color: 'green' }}>
                                    Loading
                                </Text>}
                            </Button>
                        </Form>
                    </>
                }
                </>
            )
            }
        </Container>
    );
    }
}