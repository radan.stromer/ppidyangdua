import React, { Component } from 'react';
import { View,Image,KeyboardAvoidingView,Linking,TouchableOpacity } from "react-native";
import {
  Container,
  Header,
  Content,
  Tab,
  Tabs,
  Form,
  Item,
  Input,
  Button,
  Text,
  H3,
  Textarea,
  Picker,
  Icon,
  Card,
  CardItem
} from 'native-base';
import LogoHeader2 from '../../component/LogoHeader2';
import LogoHeader from '../../component/LogoHeader'; 
import { Col, Row, Grid } from 'react-native-easy-grid';
import { ImagePicker,DocumentPicker } from 'expo';
import { withNavigation } from 'react-navigation';
import * as SecureStore from 'expo-secure-store';


function Keberatan(props){
  return (<View><CardItem header bordered style={{justifyContent:'center'}} >
                  <Text>Keberatan</Text>
                </CardItem>
                <CardItem bordered>
                  <Grid>
                    <Row>
                      <Col>
                        <Text style={{fontSize:11}}>Tanggal Pengajuan </Text>
                        <Text style={{fontSize:11}}>{ props.waktu_keberatan }</Text>
                      </Col> 
                      <Col>
                        <Text style={{fontSize:11}}>Tanggal Jawaban </Text>
                        <Text style={{fontSize:11}}>19 Maret 2019 Jam 15:30 </Text>
                      </Col>
                    </Row>
                  </Grid>
                </CardItem>
                < CardItem bordered >
                    <Text> { props.jawaban_keberatan } </Text>
                </ CardItem>
              </View>
            );
}

export default class Jawaban extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      no_pengajuan : '',
      jawaban: '',
      link_lampiran : '',
      jawaban_keberatan: '',
      waktu_keberatan: '',
      tanggal : '',
      tanggal_jawab: '',
      cara_dapat_salinan : '',
      cara_peroleh       : '',
      tujuan_pengajuan : '',
      rincian_pengajuan: '',
      status : '',
      loading: true
    };
  }

  get_pengajuan = async () =>{
    const value = await SecureStore.getItemAsync('token')
    let id_pengajuan = this.props.navigation.getParam('pengajuan', '');
    fetch(`${global.apiEndPoint}index.php/api/pengajuan/pengajuan/${id_pengajuan}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': value
      }
    })
    .then((response) => response.json())
    .then((responseJson) => {
      
      if(responseJson.status==200){
          this.setState({
              no_pengajuan: responseJson.pengajuan.no_pengajuan,
              jawaban:responseJson.pengajuan.jawaban,
              link_lampiran: responseJson.pengajuan.link_lampiran,
              link_lampiran_keberatan: responseJson.pengajuan.link_lampiran_keberatan,
              tanggal:responseJson.pengajuan.waktu,
              tanggal_jawab:responseJson.pengajuan.last_edit,
              cara_dapat_salinan: responseJson.pengajuan.cara_dapat_salinan,
              cara_peroleh: responseJson.pengajuan.cara_peroleh_info,
              tujuan_pengajuan: responseJson.pengajuan.tujuan,
              rincian_pengajuan: responseJson.pengajuan.rincian,
              status: responseJson.pengajuan.status,
              jawaban_keberatan: responseJson.pengajuan.keberatan_jwb,
              loading : false
          });
      }
    }).catch((error) => {
      console.error(error);
    });
  }

  componentDidMount() {   
      this.get_pengajuan();
  }

  handleSelesai = async ()=>{
    const value = await SecureStore.getItemAsync('token');
    fetch(`${global.apiEndPoint}index.php/api/pengajuan/pengajuan`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': value
      },
      body:JSON.stringify({ 
        'status' : 'Sudah selesai',
        'id'     :  this.props.navigation.getParam('pengajuan', '')
      })
    })
    .then((response) => response.json())
    .then((responseJson) => {
      alert('Pengajuan Anda sudah selesai');
      this.get_pengajuan();
      //this.props.navigation.navigate('Dashboarduser'); 
    }).catch((error) => {
      console.error(error);
    });
    
  }


  static navigationOptions = {
    headerTitle: <LogoHeader2 />,
    headerStyle: {
      backgroundColor:'white',
      paddingBottom:20,
      height:88
    },
  };

  render() {
    if(this.state.loading){
        return (
          <Text>Sedang memuat....</Text>
        );
      }
    
    let lampiran = '';
    if(this.state.status=='Sudah dijawab'){
       lampiran = this.state.link_lampiran.split(",");
    }

    if(this.state.status=='Keberatan sudah dijawab'){
      lampiran = this.state.link_lampiran_keberatan.split(",");
    }

    let isiLampiran = [];
    for(let i = 0; i < lampiran.length; i++){

      isiLampiran.push(
        <View key = {i}>
          <TouchableOpacity onPress={()=>{ Linking.openURL(lampiran[i])}}><Text style={{color:"blue"}}>Link lampiran {i+1}</Text></TouchableOpacity>
        </View>
      )
    }

    return (
      <Container style={{padding:5}}>
          <Content>
            <Card>
              <CardItem header bordered style={{justifyContent:'center'}} >
                <Text>{this.state.no_pengajuan}</Text>
              </CardItem>
              <CardItem bordered>
                <Grid>
                  <Row>
                    <Col>
                      <Text style={{fontSize:11}}>Tanggal Pengajuan </Text>
                      <Text style={{fontSize:11}}>{ this.state.tanggal }</Text>
                    </Col> 
                    <Col>
                      <Text style={{fontSize:11}}>Tanggal Jawaban </Text>
                      <Text style={{fontSize:11}}>{ this.state.tanggal_jawab}</Text>
                    </Col>
                  </Row>
                </Grid>
              </CardItem>
              <CardItem bordered>
                <Grid>
                  <Row>
                    <Col>
                      <Text style={{fontSize:11}}>Cara mendapatkan salinan</Text>
                      <Text style={{fontSize:11}}>{ this.state.cara_dapat_salinan }</Text>
                    </Col> 
                    <Col>
                      <Text style={{fontSize:11}}>Cara memperoleh</Text>
                      <Text style={{fontSize:11}}>{ this.state.cara_peroleh}</Text>
                    </Col>
                  </Row>
                </Grid>
              </CardItem>
              <CardItem bordered>
                  <Grid>
                  <Row>
                    <Text style={{fontSize:12,fontWeight:'bold'}}>Rincian Permohonan</Text>
                  </Row>
                  <Row>
                    <Text style={{fontSize:11}}>{this.state.rincian_pengajuan}</Text>
                  </Row>
                </Grid>
              </CardItem>
              <CardItem bordered>
                  <Grid>
                  <Row>
                    <Text style={{fontSize:12,fontWeight:'bold'}}>Tujuan Penggunaan Informasi</Text>
                  </Row>
                  <Row>
                    <Text style={{fontSize:11}}>{this.state.tujuan_pengajuan}</Text>
                  </Row>
                </Grid>
              </CardItem>
              <CardItem bordered >
                <View style={{flex: 1, flexDirection: 'column'}}>
                  <Text>{(this.state.status !="Keberatan sudah dijawab")?this.state.jawaban:this.state.jawaban_keberatan}</Text>

                  {isiLampiran}
                </View>              
              </ CardItem>
              {(this.state.jawaban_keberatan !=null)?<Keberatan waktu_keberatan={this.state.waktu_keberatan} jawaban_keberatan={this.state.jawaban_keberatan}/>:<View></View>
              }
              <CardItem footer bordered>
                <Text> Status Pengajuan: {this.state.status}</Text>
              </CardItem>
            </Card>
          </Content>
          {
            (this.state.status == "Sudah dijawab") ?
            < View >
          <Button full success onPress={this.handleSelesai}>
            <Text>Selesai</Text>
          </Button>
          <Button danger style={{marginTop: 5}} full onPress = { () => this.props.navigation.navigate('Formkeberatan', { pengajuan: this.props.navigation.getParam('pengajuan', 'ga ada') }) } >
          <Text>Ajukan Keberatan</Text>
          </Button ></View>
          : <View></View>
          }
      </Container>
    );
  }
}