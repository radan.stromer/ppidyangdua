import React from 'react';
import { Container, Content, Form, Item, Input,Button,Text } from 'native-base';
import LogoHeader from '../../component/LogoHeader';
import { Col, Grid } from 'react-native-easy-grid';
import * as SecureStore from 'expo-secure-store';

export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email:'',
      pass:'',
      errorMessage: null ,
      loading:false,
    };
  }

  static navigationOptions = {
    headerTitle: <LogoHeader />,
    headerStyle: {
      backgroundColor:'green',
      paddingBottom:20,
      height:88
    },
  }; 
 
  handleLogin = () => {
    this.setState({
      loading: true
    });
    fetch(`${global.apiEndPoint}index.php/api/login/login`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email     : this.state.email,
        password  : this.state.pass,
      })
    }).then((response) => response.json())
      .then((responseJson) => {
        if(responseJson.status==200){
          SecureStore.setItemAsync('token', responseJson.token);
          this.props.navigation.navigate('Dashboarduser'); 
        }else{
          alert(responseJson.msg);
        }
        console.log(responseJson);
      }).catch((error) => {
        this.setState({
          loading: false
        });
        alert(error);
        console.error(error);
      });
  }

  render() {
    return (
      <Container style={{padding:10}}>
                <Text style={{fontWeight:'bold', fontSize:14,marginBottom:10, textAlign:'justify'}}>Untuk mengajukan permohonan informasi atau pengajuan keberatan, silahkan login.</Text>
              {this.state.errorMessage &&
              <Text style={{ color: 'red' }}>
                {this.state.errorMessage}
              </Text>}
              <Form>
                <Item regular style={{marginBottom:15}}>
                  <Input onChangeText={email=>this.setState({email})} placeholder="Email" keyboardType="email-address"/>
                </Item>
                <Item regular style={{marginBottom:15}}>
                  <Input onChangeText={pass=>this.setState({pass})} placeholder="Password" secureTextEntry />
                </Item>
                <Button full onPress={this.handleLogin}>
                  <Text>
                    Login
                  </Text>
                </Button>
                {this.state.loading &&
              <Text style={{ color: 'green' }}>
                Loading
              </Text>}
              </Form>
              <Grid style={{marginTop:20}}>
                <Col>
                  <Button full bordered onPress={() => this.props.navigation.navigate('Signup')}>
                    <Text>
                      Daftar
                    </Text>
                  </Button>
                </Col>
                <Col>
                  <Button full bordered onPress={() => this.props.navigation.navigate('Lupa')}>
                    <Text>
                      Lupa Password
                    </Text>
                  </Button>
                </Col>
              </Grid>
      </Container>
    );
  }
}