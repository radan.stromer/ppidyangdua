import React, { Component } from 'react';
import { View } from "react-native";
import { Container, Content,Button,Text, Card, CardItem, Body } from 'native-base';
import LogoHeader2 from '../../component/LogoHeader2';
import { Col, Grid } from 'react-native-easy-grid';
import * as SecureStore from 'expo-secure-store';
import Moment from 'moment';


function Footer1(props) {
  return (<Grid>
            <Col>
              <Button full onPress={() => props.nav.navigate('Jawaban',{pengajuan:props.noaju,user:props.user})}>
                <Text>Lihat Jawaban </Text>
              </Button>
            </Col>
          </Grid>);
}

function Footer2(props) {
  return (<Grid>
            <Col>
              <Button full onPress={() => props.nav.navigate('Jawaban',{pengajuan:props.noaju,user:props.user})}>
                <Text>Lihat Jawaban Keberatan</Text>
              </Button>
            </Col>
          </Grid>);
}

function Footerused(props){
    console.log(props);
    if(props.data.status == "Sudah dijawab"){
            return (<Footer1 nav={props.nav} noaju={props.noaju} user={props.user}/>);
    }else if(props.data.status == "Keberatan sudah dijawab"){
            return (<Footer2 nav={props.nav} noaju={props.noaju} user={props.user} / >);
    }else{
      let tampilan_footer = <View><Text>{props.data.status}</Text></View>;
      if(props.data.kurang_lengkap=='Y'){
        tampilan_footer = <View>
                  <Text>Permohonan informasi Anda kurang lengkap</Text>
                  <Text>{props.data.jawaban}</Text>
                </View>;
      }
      if(props.data.info_dikecualikan=='Y'){
        tampilan_footer = <View>
                  <Text>Permohonan informasi Anda dikecualikan</Text>
                  <Text>{props.data.jawaban}</Text>
                </View>;
      }
      return (tampilan_footer);
    }
}


export default class Dashboarduser extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user : '',
      listpengajuan : null,
      loading: true,
      token :''
    };
  }

  static navigationOptions = {
    headerTitle: <LogoHeader2 />,
    headerStyle: {
      backgroundColor:'white',
      paddingBottom:20,
      height:88
    },
  };

  get_list_pengajuan = async () =>{
    const value = await SecureStore.getItemAsync('token')

      if(value){
        fetch(`${global.apiEndPoint}index.php/api/pengajuan/pengajuan`, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': value
          }
        })
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
          if(responseJson.status==200){
            this.setState({listpengajuan:responseJson.pengajuan});
            this.setState({loading:false});
          } 
        }).catch((error) => {
          console.error(error);
        });
      }
  }

  componentDidMount() {
    this.get_list_pengajuan();
  }

  render() {
    if(this.state.loading){
        return (
          <Text>Sedang memuat....</Text>
        );
      }
    const Moment = require('moment');
    let indonesia = require('moment/locale/id');
    Moment.updateLocale('id', indonesia);
    return (
      <Container style={{padding:6}}>
          <Button full onPress={() => this.props.navigation.navigate('Formpengajuan')}>
            <Text>
              Ajukan Permohonan Informasi
            </Text>
          </Button>
          
          {this.state.listpengajuan && this.state.listpengajuan.map((data,key) => {
            return (<Card>
                      <CardItem header bordered>
                        <View>
                          <Text>Pengajuan {data.no_pengajuan}</Text>
                          <Text>{Moment(data.waktu).format(' Do MMMM YYYY HH:mm ')}</Text>
                        </View>
                      </CardItem>
                      <CardItem bordered>
                        <Body>
                          <Text style={{fontWeight:'bold'}}>
                            Rincian : 
                          </Text>
                          <Text>
                            {data.rincian}
                          </Text>
                        </Body>
                      </CardItem>
                      <CardItem bordered>
                        <Body>
                          <Text style={{fontWeight:'bold'}}>
                            Tujuan :
                          </Text>
                          <Text>
                            {data.tujuan}
                          </Text>
                          </Body>
                      </CardItem>
                      <CardItem bordered>
                      <Body>
                          <Text style={{fontWeight:'bold'}}>
                            Cara mendapatkan salinan : 
                          </Text>
                          <Text>
                            {data.cara_dapat_salinan}
                          </Text>
                          </Body>
                      </CardItem>
                      <CardItem bordered>
                        <Body>
                          <Text style={{fontWeight:'bold'}}>
                            Cara memperoleh :
                          </Text>
                          <Text>
                            {data.cara_peroleh_info}
                          </Text>
                        </Body>
                          </CardItem>
                          <CardItem footer bordered>
                            <Footerused nav={this.props.navigation} data={data} noaju={data.id} user={this.state.user} />
                          </CardItem>
                    </Card>
                    )
          })}
      </Container>
    );
  }
}