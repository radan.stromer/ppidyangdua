import React, { Component } from 'react';
import { View,Image,KeyboardAvoidingView,SafeAreaView } from "react-native";
import { Container, Content, Form, Item, Button,Text,Textarea,Picker } from 'native-base';
import LogoHeader2 from '../../component/LogoHeader2';
import { Col, Grid } from 'react-native-easy-grid';
import * as ImagePicker from 'expo-image-picker';
import * as DocumentPicker from 'expo-document-picker';
import * as SecureStore from 'expo-secure-store';
import Moment from 'moment';
import GoogleReCaptcha from 'rn-google-recaptcha-v2';
const siteKey = '6LfZFdkUAAAAALib79ReeaQafUOl3u1ONFH-YO9N';
const baseUrl = 'http://dadan.id';


export default class Formpengajuan extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selected2: '',
      selected3: '',
      image: null,
      dokumen: null,
      dokumen_ext : null,
      dokumen_name : null,
      rincian : '',
      tujuan : '',
      user : '',
      bulan_pengajuan : '',
      tahun_pengajuan: '',
      no_pengajuan : '',
      user_name : '',
      recaptchaViewHeight: 90, //initial default height
      showSubmit : false,
      token:''

    };
  }

  async componentDidMount() {
    let value = await SecureStore.getItemAsync('token')
    this.setState({token : value});
  }

  romanize(num) {
    if (isNaN(num))
      return NaN;
    var digits = String(+num).split(""),
      key = ["", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM",
        "", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC",
        "", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"
      ],
      roman = "",
      i = 3;
    while (i--)
      roman = (key[+digits.pop() + (i * 10)] || "") + roman;
    return Array(+digits.join("") + 1).join("M") + roman;
  }

   onValueChange2(value: string) {
    this.setState({
      selected2: value
    });
  }

   onValueChange3(value: string) {
    this.setState({
      selected3: value
    });
  }

  static navigationOptions = {
    headerTitle: <LogoHeader2 />,
    headerStyle: {
      backgroundColor:'white',
      paddingBottom:20,
      height:88
    },
  };

  _pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: false
    });

    if (!result.cancelled) {
      this.setState({ image: result.uri });
    }
  };

  _pickDoc = async () => {
    let result = await DocumentPicker.getDocumentAsync();
    let name_doc = result.name;
    let res = name_doc.split(".");
    let ekstensiDoc = res[res.length - 1];
    if (!result.cancelled) {
      this.setState({ 
        dokumen: result.uri,
        dokumen_name: result.name,
        dokumen_ext: ekstensiDoc
      });
    }
  };

  kirimPengajuan = async () => {
      //cek no pengajuan
      if(this.state.selected2=='')alert('Anda harus pilih cara memperoleh informasi');
      else if (this.state.selected3 == '') alert('Anda harus pilih cara mendapatkan salinan');
      else if (this.state.rincian == '') alert('Isian rincian informasi yang dibutuhkan harus di isi');
      else if (this.state.tujuan == '') alert('Isian tujuan penggunaan harus di isi');
      //else if (this.state.dokumen == null) alert('Dokumen upload harus disertakan');
      else if (this.state.image == null) alert('File KTP harus disertakan');
      else {
        let indonesia = require('moment/locale/id');
        Moment.updateLocale('id', indonesia);
        let bulan = parseInt(Moment().format('M'));
        let tahun = Moment().format('Y');
        Moment.updateLocale('id', indonesia);

        fetch(`${global.apiEndPoint}index.php/api/pengajuan/pengajuan`, {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': this.state.token
          },
          body: JSON.stringify({
                  rincian : this.state.rincian,
                  tujuan : this.state.tujuan,
                  cara_peroleh_info : this.state.selected2,
                  cara_dapat_salinan : this.state.selected3,
                  waktu : Moment().format('YYYY-MM-DD HH:mm:ss'),
          })
        }).then((response) => response.json())
          .then((responseJson) => {
            if(responseJson.status==200){
              this.uploadImage(this.state.image, responseJson.id_pengajuan);
              this.uploadDoc(this.state.dokumen, responseJson.id_pengajuan).then((responsedoc) => responsedoc.json()).then((responseJson2) => {
                console.log(responseJson2);
              });
              this.props.navigation.navigate('Dashboarduser'); 
            }else{
              alert(responseJson.msg);
            }
          }).catch((error) => {
            alert(error);
            console.error(error);
          });
  }
  }

  uploadImage = async (uri,key) =>{
    let apiUrl = `${global.apiEndPoint}index.php/api/pengajuan/uploadktp`;

    let uriParts = uri.split('.');
    let fileType = uriParts[uriParts.length - 1];

    let formData = new FormData();
    formData.append('ktp', {
      uri: uri,
      name: `ktp-${key}.${fileType}`,
      type: `image/${fileType}`,
    });

    let options = {
      method: 'POST',
      body: formData,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
        'Authorization': this.state.token
      },
    };

    return fetch(apiUrl, options).then((response) => response.json()).then((responseJson) => {console.log(responseJson);});
  }

  uploadDoc = async (uri, key) => {
   
    let apiUrl = `${global.apiEndPoint}index.php/api/pengajuan/uploaddoc`;

    let uriParts = uri.split('.');
    let fileType = uriParts[uriParts.length - 1];

    let formData = new FormData();
    formData.append('doc', {
      uri : uri,
      name: `doc-${key}.${fileType}`,
      type: `image/${fileType}`,
    });

    let options = {
      method: 'POST',
      body: formData,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
        'Authorization': this.state.token
      },
    };

    return fetch(apiUrl, options);
  }

  render() {
    let { image } = this.state;
    let { dokumen } = this.state;
    const { recaptchaViewHeight } = this.state;
    return (
      <Container style={{padding:10}}>
          <KeyboardAvoidingView behavior="padding" style={{ flex: 1 }} enabled>
            <Form>
              <Textarea onChangeText={rincian=>this.setState({rincian})} rowSpan={5} bordered placeholder="Rincian Informasi yang dibutuhkan" />
              <Textarea onChangeText={tujuan=>this.setState({tujuan})} rowSpan={5} bordered placeholder="Tujuan penggunaan informasi" />

              <View style={{marginVertical:10}}>
                <Text>Cara memperoleh informasi (*)</Text>
                <Item picker>
                  <Picker
                    mode="dropdown"
                    placeholder="Cara memperoleh Informasi"
                    placeholderStyle={{ color: "#bfc6ea" }}
                    placeholderIconColor="#007aff"
                    selectedValue={this.state.selected2}
                    onValueChange={this.onValueChange2.bind(this)}
                  >
                    <Picker.Item label="Tentukan Cara peroleh informasi" value="" />
                    <Picker.Item label="Melihat" value="Melihat" />
                    <Picker.Item label="Membaca" value="Membaca" />
                    <Picker.Item label="Mencatat" value="Mencatat" />
                    <Picker.Item label="Mendengarkan" value="Mendengarkan" />
                    <Picker.Item label="Mendapatkan Salinan Informasi (Soft/Hard Copy)" value="Mendapatkan Salinan" />
                  </Picker>
                </Item>
              </View>

              <View style={{marginVertical:10}}>
                <Text>Cara mendapatkan salinan (*)</Text>
                <Item picker>
                  <Picker
                    mode="dropdown"
                    placeholder="Cara mendapatkan salinan"
                    placeholderStyle={{ color: "#bfc6ea" }}
                    placeholderIconColor="#007aff"
                    selectedValue={this.state.selected3}
                    onValueChange={this.onValueChange3.bind(this)}
                  >
                    <Picker.Item label="Tentukan Cara dapatkan salinan" value="" />
                    <Picker.Item label="Mengambil langsung" value="Mengambil langsung" />
                    <Picker.Item label="Kurir Pos" value="Kurir Pos" />
                    <Picker.Item label="Pos" value="Pos" />
                    <Picker.Item label="Faksimili" value="Faksimili" />
                    <Picker.Item label="Email" value="Email" />
                    <Picker.Item label="Aplikasi" value="Aplikasi" />
                  </Picker>
                </Item>
              </View>
              <Grid>
                <Col>
                  {image && (
                  <Image source={{uri:image}} style={{ width: 100, height: 100 }} />)}
                  <Button full bordered onPress={this._pickImage}>
                    <Text>Upload KTP</Text>
                  </Button>
                </Col>
                <Col>
                  {dokumen && (
                      <View>
                        <Image source={require('../../assets/default-doc.png')} style={{ width: 100, height: 100 }} />
                        <Text>{this.state.dokumen_name}</Text>
                      </View>
                    )}
                    <Button full bordered onPress={this._pickDoc}>
                      <Text>Upload Dokumen</Text>
                    </Button>
                    </Col>
              </Grid>

              <SafeAreaView style={{flex:1}}>
                      <GoogleReCaptcha
                          style={{ height: recaptchaViewHeight }}
                          siteKey={siteKey}
                          url={baseUrl}
                          languageCode="en"
                          onMessage={this.onRecaptchaEvent} />
              </SafeAreaView>

              {this.state.showSubmit &&
              <Button style={{marginTop:20}} full onPress={this.kirimPengajuan}>
                <Text>Kirim Pengajuan</Text>
              </Button>
              }

            </Form>
          </KeyboardAvoidingView>
      </Container>
    );
  }
  onRecaptchaEvent = event => {
    if (event && event.nativeEvent.data) {
        const data = decodeURIComponent(
            decodeURIComponent(event.nativeEvent.data),
        );
        if (data.startsWith('CONTENT_PARAMS:')) {
            let params = JSON.parse(data.substring(15));
            let recaptchaViewHeight = params.visibility === 'visible' ? params.height : 90;
            this.setState({ recaptchaViewHeight });
        } else if (['cancel', 'error', 'expired'].includes(data)) {
            return;
        } else {
            console.log('Verified code from Google', data);
            this.setState({ recaptchaToken: data });
            this.setState({ showSubmit: true });
        }
    }
  };
}