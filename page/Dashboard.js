import React from "react";
import { StyleSheet, View,Image,ImageBackground,Dimensions,TouchableOpacity,Linking  } from "react-native";
import { Container, Header, Content, Button, Text, Card, CardItem, Icon, H1,Footer, FooterTab } from 'native-base';
import * as Font from 'expo-font';
import { Ionicons } from '@expo/vector-icons';
import { Col, Row, Grid } from 'react-native-easy-grid';
import LogoHeaderLink from '../component/LogoHeaderLink';

export default class Dashboard extends React.Component {
  static navigationOptions = {
    headerTitle: () => <LogoHeaderLink />,
    headerStyle: {
      backgroundColor:'white',
      paddingBottom:20,
      height:88
    },
  };

  constructor(props) {
    super(props);
    this.state = { loading: true };
  }

  async componentDidMount() {
    await Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf')
    });
    this.setState({ loading: false });
  }
  
  render() {
    if (this.state.loading) {
      return (
        <Text>Sedang memuat....</Text>
      );
    }

    const dimensions = Dimensions.get('window');
    const imageHeight = Math.round(dimensions.width * 5 / 16);
    const imageWidth = dimensions.width;
    return (
      <Container>
        <Grid>
          <Row style={{ height:120,marginTop:10}}>
            <Col style={styles.kolIcon}>
                 <TouchableOpacity onPress={() => this.props.navigation.navigate('Profil')}>
                  <Image style={styles.gambarIkon} source={require('../assets/iconppid/profileppid2.png')} /> 
                  <Text style={styles.textIkon}>Seputar PPID</Text>
                 </TouchableOpacity>
            </Col>
            <Col style={styles.kolIcon}>
              <TouchableOpacity onPress={
                  () => {this.props.navigation.navigate('Tab1pub',{
                          'sub_category' : 5
                        })
                }}>
                <Image style={styles.gambarIkon} source={require('../assets/iconppid/Regulasi2.png')} />
                <Text style={styles.textIkon}>Peraturan</Text>
              </TouchableOpacity>
            </Col>
          </Row>
          <Row style={{ height:120}}>
            <Col style={styles.kolIcon}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Publik')}>
                 <Image style={styles.gambarIkon} source={require('../assets/iconppid/informasipublik2.png')} />
                 <Text style={styles.textIkon}>Informasi Publik</Text>
                </TouchableOpacity>
            </Col>
            <Col style={styles.kolIcon}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Layanan')}>
                  <Image style={styles.gambarIkon} source={require('../assets/iconppid/standardlayanan2.png')} />
                  <Text style={styles.textIkon}>Media Layanan</Text>
                </TouchableOpacity>
            </Col>
          </Row>
           <Row style={{ height:120 }}>
            <Col style={styles.kolIcon}>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')}>
                <Image style={styles.gambarIkon} source={require('../assets/iconppid/layananPPID2.png')} />
                <Text style={styles.textIkon}>Layanan Informasi</Text>
              </TouchableOpacity>
            </Col>
            <Col style={styles.kolIcon}>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('Faq')}>
                <Image style={styles.gambarIkon} source={require('../assets/iconppid/Q&A2.png')} />
                <Text style={styles.textIkon}>Q & A</Text>
              </TouchableOpacity>
            </Col>
          </Row>
        </Grid>

       
        <Footer>
          <FooterTab style={{backgroundColor:'#16529b'}}>
            <Button onPress={ ()=>{ Linking.openURL('https://www.facebook.com/bp2mi.ri/')}}>
              <Icon type="FontAwesome" name="facebook" />
            </Button>
            <Button onPress={ ()=>{ Linking.openURL('https://www.instagram.com/bp2mi_ri')}}>
              <Icon type="FontAwesome" name="instagram" />
            </Button>
            <Button onPress={ ()=>{ Linking.openURL('https://twitter.com/bp2mi_ri')}}>
              <Icon type="FontAwesome" name="twitter" />
            </Button>
            <Button onPress={ ()=>{ Linking.openURL('https://www.youtube.com/channel/UC42wXrFJ7-fmsSHw_MqbWaA')}}>
              <Icon type="FontAwesome" name="youtube" />
            </Button>
            
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  Ikon: {
    fontSize: 50,
    textAlign:'center',
    padding:5
  },
  kolIcon:{
    justifyContent: 'center',
    alignItems: 'center',
  },
  gambarIkon:{
    width: 80, 
    height: 80,
    borderRadius: 40,
    borderColor:'black',
    borderWidth:1,
    textAlign: 'center',
    marginLeft: 20,
  },
  textIkon:{
    width:120,
    textAlign:'center'
  }
});