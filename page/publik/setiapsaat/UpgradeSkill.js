import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { Container, Content, Text } from 'native-base';
import PDFReader from 'rn-pdf-reader-js';
import { Constants } from 'expo';


export default class UpgradeSkill extends React.Component {
  render() {
    return (
      <Container>
        <PDFReader
          source={{uri:'http://dadan.id/DEPUTIBIDANGKLNP_UPGRADINGSKILLS2018.pdf'}}
        />
      </Container>
    );
  }
}