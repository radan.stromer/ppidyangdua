import React, { Component } from 'react';
import { Container } from 'native-base';
import PDFReader from 'rn-pdf-reader-js';

export default class Berkala1 extends React.Component {

  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('nama',''),
    };
  };

  render() {
    const { navigation } = this.props;
    console.log(`${global.apiEndPoint}assets/upload/infopublic/${navigation.getParam('file', '')}`);
    return (
      <Container>
        <PDFReader
          source={{uri:`${global.apiEndPoint}assets/upload/infopublic/${navigation.getParam('file', '')}`}}
        />
      </Container>
    );
  }
}