import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity,ScrollView } from 'react-native';
import { Container, Content, Text, Button,H3, List, ListItem } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { withNavigation } from 'react-navigation';
import LogoHeader2 from '../../component/LogoHeader2';

class Tab1 extends React.Component {
  static navigationOptions = {
    headerTitle: <LogoHeader2 />,
    headerStyle: {
      backgroundColor:'white',
      paddingBottom:20,
      height:88
    },
  };

  constructor(props) {
	super(props);
	this.state = {
		listdata : null,
		loading : true
	}
  }

  async componentDidMount() {
	fetch(`${global.apiEndPoint}index.php/api/infopublic/info_public/${this.props.navigation.getParam('sub_category', 'NO-ID')}`, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Accept': 'application/json',
		}
	})
	.then((response) => response.json())
	.then((responseJson) => {
		if(responseJson.status==200){
			this.setState({listdata:responseJson.data});
			this.setState({loading:false});
		}
		console.log(responseJson.data);
	}).catch((error) => {
		console.error(error);
	});
	
  }

  render() {
	if(this.state.loading){
		return (
			<Text>Sedang memuat....</Text>
		);
	}
    return (
      	<Container style={{padding:10}}>
			<ScrollView>
					{this.state.listdata && this.state.listdata.map((datasub,keysub) => {
						return(
							<List>
								<ListItem itemDivider style={{flex:1,alignItems:'flex-start',justifyContent:"flex-start"}}>
									<View style={{width:'7%'}}>
										<Text style={{marginRight:5,fontWeight:'bold', fontSize:13}}> {datasub.urutan_abjad}.</Text> 
									</View>
									<View style={{width:'65%'}}>
										<Text style={{width:'100%',fontWeight:'bold', fontSize:13}}>{datasub.title}</Text>
									</View>
								</ListItem>
								{datasub.item.map((dataitem,keyitem)=>{
									return (
										<ListItem style={{paddingBottom: 1, flex:1,alignItems:'flex-start'}}>
											<View style={{width:'5%'}}>
												<Text style={{fontSize:14}}>{keyitem+1}.</Text>
											</View>
											<View style={{width:'70%'}}>
												<Text style={{width:'100%',fontSize:14}}>{dataitem.title}</Text>
											</View>
											
											<View style={{
											flex: 1,
											flexDirection: 'row',
											justifyContent: 'flex-end',
											}}>
											<TouchableOpacity onPress={() => this.props.navigation.navigate('Berkala1',{file:dataitem.file,nama:dataitem.title})} style={{backgroundColor:'green',borderRadius:2, paddingBottom:2,paddingTop:2,height:23,width:50}}>
												<Text style={{fontSize:12,textAlign:'center',color:'white'}}>
												View
												</Text>
											</TouchableOpacity>
											</View>
										</ListItem>
									);
									
								})}
							</List>
						);
					})}
	        </ScrollView>
        </Container>
    );
  }
}

export default withNavigation(Tab1);