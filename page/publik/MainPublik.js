import React, { Component } from 'react';
import { Container, Header, Content, Button,Text } from 'native-base';
import LogoHeader2 from '../../component/LogoHeader2';
export default class MainPublik extends React.Component {
  constructor(props) {
    super(props);
  }

  static navigationOptions = {
    headerTitle: <LogoHeader2 />,
    headerStyle: {
      backgroundColor:'green',
      paddingBottom:20,
      height:88
    },
  };

  render() {
    return (
      <Container>
        <Content style={{padding:10}}>
          <Button onPress={
            () => {this.props.navigation.navigate('Tab1pub',{
                    'sub_category' : 1
                  })
          }} warning full style={{marginBottom:10}}>
            <Text style={{color:'black'}}>
              Informasi Berkala
            </Text>
          </Button>
          <Button onPress={
            () => {this.props.navigation.navigate('Tab1pub',{
                    'sub_category' : 2
                  })
          }} warning full style={{marginBottom:10}}>
            <Text style={{color:'black'}}>
              Informasi Serta Merta
            </Text>
          </Button>
          <Button onPress={
            () => {this.props.navigation.navigate('Tab1pub',{
                    'sub_category' : 3
                  })
          }} warning full style={{marginBottom:10}}>
            <Text style={{color:'black'}}>
             Informasi Tersedia Setiap Saat
            </Text>
          </Button>
          <Button onPress={
            () => {this.props.navigation.navigate('Tab1pub',{
                    'sub_category' : 4
                  })
          }} warning full style={{marginBottom:10}}>
            <Text style={{color:'black'}}>
              Laporan
            </Text>
          </Button>
        </Content>
      </Container>
    );
  }
}