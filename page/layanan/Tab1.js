import React, { Component } from 'react';
import { Image, ScrollView ,TouchableHighlight, Dimensions } from "react-native";
import { Container, Content, Text,View,List,ListItem } from 'native-base';
import ImageZoom from 'react-native-image-pan-zoom';
import LogoHeader2 from '../../component/LogoHeader2'; 
export default class Tab1 extends React.Component {
	static navigationOptions = {
    headerTitle: <LogoHeader2 />,
    headerStyle: {
      backgroundColor:'white',
      paddingBottom:20,
      height:88
    },
  };

	static defaultProps = {
	  doAnimateZoomReset: false,
	  maximumZoomScale: 2,
	  minimumZoomScale: 1,
	  zoomHeight: Dimensions.get('window').height, 
	  zoomWidth: Dimensions.get('window').width,
	}
	handleResetZoomScale = (event) => {
	  this.scrollResponderRef.scrollResponderZoomTo({ 
	     x: 0, 
	     y: 0, 
	     width: this.props.zoomWidth, 
	     height: this.props.zoomHeight, 
	     animated: true 
	  })
	}
	setZoomRef = node => { //the ScrollView has a scrollResponder which allows us to access more methods to control the ScrollView component
	  if (node) {
	    this.zoomRef = node
	    this.scrollResponderRef = this.zoomRef.getScrollResponder()
	  }
	}

	render() {
		return (
			<View>
		  <ImageZoom style={{marginTop: -65,}} cropWidth={Dimensions.get('window').width}
        			 cropHeight={Dimensions.get('window').height}
        			 imageWidth={345}
        			 imageHeight={510}
        			 enableSwipeDown={true}>
		    		<Image enableHorizontalBounce={true} style={{width:345, height:510}} source={require('../../assets/ProsedurPermohonanInformasi.jpg')} /> 
		    	</ImageZoom>
			</View>
		);
	}
}