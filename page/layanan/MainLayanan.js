import React, { Component } from 'react';
import { Container, Header, Content, Tab, Tabs,Button,Text } from 'native-base';
import LogoHeader from '../../component/LogoHeader';
export default class MainLayanan extends React.Component {
  static navigationOptions = {
    headerTitle: <LogoHeader />,
    headerStyle: {
      backgroundColor:'white',
      paddingBottom:20,
      height:88
    },
  };

  render() {
    return (
      <Container style={{padding:10}}>
          <Button onPress={() => this.props.navigation.navigate('Layanan1')} warning full style={{marginBottom:10}}>
            <Text style={{color:'black'}}>
              Alur Permohonan Informasi
            </Text>
          </Button>
          <Button onPress={() => this.props.navigation.navigate('Layanan3')} warning full style={{marginBottom:10}}>
            <Text style={{color:'black'}}>
              Jalur Layanan
            </Text>
          </Button>
          <Button onPress={() => this.props.navigation.navigate('Layanan4')} warning full style={{marginBottom:10}}>
            <Text style={{color:'black'}}>
              Waktu Layanan
            </Text>
          </Button>
          <Button onPress={() => this.props.navigation.navigate('Layanan5')} warning full style={{marginBottom:10}}>
            <Text style={{color:'black'}}>
              Biaya Layanan
            </Text>
          </Button>
          <Button onPress={() => this.props.navigation.navigate('Layanan6')} warning full style={{marginBottom:10}}>
            <Text style={{color:'black'}}>
              Call Center
            </Text>
          </Button>
          <Button onPress={() => this.props.navigation.navigate('Layanan7')} warning full style={{marginBottom:10}}>
            <Text style={{color:'black'}}>
              Maklumat Pelayanan Publik
            </Text>
          </Button>
      </Container>
    );
  }
}