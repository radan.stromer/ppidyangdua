import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Content, Text,H3,List, ListItem } from 'native-base';
import LogoHeader2 from '../../component/LogoHeader2'; 
export default class Tab3 extends React.Component {
  static navigationOptions = {
    headerTitle: <LogoHeader2 />,
    headerStyle: {
      backgroundColor:'white',
      paddingBottom:20,
      height:88
    },
  };
  render() {
    return (
			<Image style={{right:40,top:5,width:480, height:600}} source={require('../../assets/jalur-layanan.png')}/>
		);
  }
}