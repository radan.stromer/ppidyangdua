import React, { Component } from 'react';
import { StyleSheet, View,TouchableOpacity,Image } from 'react-native';
import { Container, Content, Text,H3,List, ListItem } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import LogoHeader2 from '../../component/LogoHeader2'; 
export default class Tab5 extends React.Component {
  static navigationOptions = {
    headerTitle: <LogoHeader2 />,
    headerStyle: {
      backgroundColor:'white',
      paddingBottom:20,
      height:88
    },
  };
  render() {
    return (
      <View style={{flex:1, flexDirection: 'column', justifyContent:'flex-start'}}>
        <Image style={{right:40,bottom:55,width:470, height:600}} source={require('../../assets/biayalayanan.png')}/>
      </View>
    );
  }
}